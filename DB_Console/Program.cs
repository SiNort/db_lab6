﻿using DatabaseLibrary;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using Procedures = DatabaseLibrary.DatabaseCommands.Procedures;
using Tables = DatabaseLibrary.DatabaseCommands.Tables;

namespace DB_Console
{
    class Program
    {
        static void Main(string[] args)
        {
            int choice = 0, tableNum, rowNum;
            List<object[]> data;
            List<object> carParams;
            Database database = new Database(ConfigurationManager.ConnectionStrings["DB"].ConnectionString);
            DatabaseInitializer.Initialize(ConfigurationManager.ConnectionStrings["DB"].ConnectionString);

            while (true)
            {
                tableNum = 0;
                rowNum = 0;
                data = null;
                carParams = null;
                Console.WriteLine("------------Menu------------");
                Console.WriteLine("1) Select data from table");
                Console.WriteLine("2) Employee info");
                Console.WriteLine("3) Get selled cars info");
                Console.WriteLine("4) Get orders by condition(< 200 and > 100)");
                Console.WriteLine("5) Get emploees by post");
                Console.WriteLine("6) Get cars by manufacturer");
                Console.WriteLine("7) Get orders dates and price");
                Console.WriteLine("8) Get all selled cars manufactured in specific country");
                Console.WriteLine("9) Add Car");
                Console.WriteLine("10) Update Car");
                Console.WriteLine("11) Delete Car");
                Console.WriteLine("12) Exit");
                Console.Write("Enter: ");
                choice = Convert.ToInt32(Console.ReadLine());
                switch (choice)
                {
                    case 1:
                        var tables = Enum.GetValues(typeof(Tables)).Cast<Tables>().ToList();
                        Console.WriteLine("---Tables---");
                        for (int i = 0; i < tables.Count(); i++)
                        {
                            Console.WriteLine($"{i + 1}) {tables[i]}");
                        }
                        Console.Write("Table number: ");
                        tableNum = Convert.ToInt32(Console.ReadLine());
                        Console.Write("Number of rows: ");
                        rowNum = Convert.ToInt32(Console.ReadLine());

                        data = database.Select((Tables)tableNum - 1, rowNum);

                        for (int i = 0; i < data.Count; i++)
                        {
                            for (int j = 0; j < data[i].Length; j++)
                            {
                                Console.Write($"| {data[i][j]} ");
                            }
                            Console.Write("\n");
                        }
                        break;
                    case 2:
                        Console.WriteLine("---Employee---");
                        data = database.Select(Tables.Employees);

                        for (int i = 0; i < data.Count; i++)
                        {
                            for (int j = 0; j < data[i].Length; j++)
                            {
                                Console.Write($"| {data[i][j]} ");
                            }
                            Console.Write("\n");
                        }
                        Console.ReadKey();
                        Console.WriteLine("---Duties---");
                        data = database.Select(Tables.Duties);

                        for (int i = 0; i < data.Count; i++)
                        {
                            for (int j = 0; j < data[i].Length; j++)
                            {
                                Console.Write($"| {data[i][j]} ");
                            }
                            Console.Write("\n");
                        }
                        Console.ReadKey();
                        Console.WriteLine("---Requirements---");
                        data = database.Select(Tables.Requirements);

                        for (int i = 0; i < data.Count; i++)
                        {
                            for (int j = 0; j < data[i].Length; j++)
                            {
                                Console.Write($"| {data[i][j]} ");
                            }
                            Console.Write("\n");
                        }
                        break;
                    case 3:
                        Console.WriteLine("---Selled cars data---");
                        data = database.SelectProcedure(Procedures.GetSelledCars, null);

                        for (int i = 0; i < data.Count; i++)
                        {
                            for (int j = 0; j < data[i].Length; j++)
                            {
                                Console.Write($"| {data[i][j]} ");
                            }
                            Console.Write("\n");
                        }
                        break;
                    case 4:
                        Console.WriteLine("---Orders---");
                        data = database.SelectProcedure(Procedures.GetOrders100and200, null);

                        for (int i = 0; i < data.Count; i++)
                        {
                            for (int j = 0; j < data[i].Length; j++)
                            {
                                Console.Write($"| {data[i][j]} ");
                            }
                            Console.Write("\n");
                        }
                        break;
                    case 5:
                        Console.Write("Ender postId: ");
                        int postId = Convert.ToInt32(Console.ReadLine());
                        Console.WriteLine("---Employee---");
                        data = database.SelectProcedure(Procedures.GetEmployeeByPost, new object[] { postId });

                        for (int i = 0; i < data.Count; i++)
                        {
                            for (int j = 0; j < data[i].Length; j++)
                            {
                                Console.Write($"| {data[i][j]} ");
                            }
                            Console.Write("\n");
                        }
                        break;
                    case 6:
                        Console.Write("Ender manufacturerId: ");
                        int manufacturerId = Convert.ToInt32(Console.ReadLine());
                        Console.WriteLine("---Cars---");
                        data = database.SelectProcedure(Procedures.GetCarsByManufacturer, new object[] { manufacturerId });

                        for (int i = 0; i < data.Count; i++)
                        {
                            for (int j = 0; j < data[i].Length; j++)
                            {
                                Console.Write($"| {data[i][j]} ");
                            }
                            Console.Write("\n");
                        }
                        break;
                    case 7:
                        Console.WriteLine("---Orders prices---");
                        data = database.SelectProcedure(Procedures.GetOrdersPricesAndDates, null);

                        for (int i = 0; i < data.Count; i++)
                        {
                            for (int j = 0; j < data[i].Length; j++)
                            {
                                Console.Write($"| {data[i][j]} ");
                            }
                            Console.Write("\n");
                        }
                        break;
                    case 8:
                        Console.WriteLine("Ender country id: ");
                        int countryId = Convert.ToInt32(Console.ReadLine());
                        string price = (string)database.ScalarProcedure(Procedures.GetCarsPriceByCountry, new object[] { countryId });
                        Console.WriteLine($"Price sum: {price}");
                        break;
                    case 9:
                        try
                        {
                            Console.WriteLine("Production date: ");
                            carParams.Add(DateTime.Parse(Console.ReadLine()));
                            Console.WriteLine("Engine num: ");
                            carParams.Add(Console.ReadLine());
                            Console.WriteLine("Price: ");
                            carParams.Add(float.Parse(Console.ReadLine()));
                            Console.WriteLine("Model id: ");
                            carParams.Add(Convert.ToInt32(Console.ReadLine()));
                            Console.WriteLine("Manufacturer id: ");
                            carParams.Add(Convert.ToInt32(Console.ReadLine()));
                            Console.WriteLine("Case id: ");
                            carParams.Add(Convert.ToInt32(Console.ReadLine()));
                            Console.WriteLine("ColorId: ");
                            carParams.Add(Convert.ToInt32(Console.ReadLine()));
                            Console.WriteLine("Specification id: ");
                            carParams.Add(Convert.ToInt32(Console.ReadLine()));
                            database.SelectProcedure(Procedures.AddCar, carParams.ToArray());
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine("Error: " + ex.Message);
                        }
                        break;
                    case 10:
                        try
                        {
                            Console.WriteLine("Enter car id: ");
                            int id = Convert.ToInt32(Console.ReadLine());
                            data = database.SelectById(Tables.Cars, id);

                            for (int i = 0; i < data.Count; i++)
                            {
                                for (int j = 0; j < data[i].Length; j++)
                                {
                                    Console.Write($"| {data[i][j]} ");
                                }
                                Console.Write("\n");
                            }

                            Console.WriteLine("Production date: ");
                            carParams.Add(DateTime.Parse(Console.ReadLine()));
                            Console.WriteLine("Engine num: ");
                            carParams.Add(Console.ReadLine());
                            Console.WriteLine("Price: ");
                            carParams.Add(float.Parse(Console.ReadLine()));
                            Console.WriteLine("Model id: ");
                            carParams.Add(Convert.ToInt32(Console.ReadLine()));
                            Console.WriteLine("Manufacturer id: ");
                            carParams.Add(Convert.ToInt32(Console.ReadLine()));
                            Console.WriteLine("Case id: ");
                            carParams.Add(Convert.ToInt32(Console.ReadLine()));
                            Console.WriteLine("ColorId: ");
                            carParams.Add(Convert.ToInt32(Console.ReadLine()));
                            Console.WriteLine("Specification id: ");
                            carParams.Add(Convert.ToInt32(Console.ReadLine()));
                            database.SelectProcedure(Procedures.AddCar, carParams.ToArray());
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine("Error: " + ex.Message);
                        }
                        break;
                    case 11:
                        Console.WriteLine("Enter car id: ");
                        carParams.Add(Convert.ToInt32(Console.ReadLine()));
                        database.SelectProcedure(Procedures.DeleteCar, carParams.ToArray());
                        break;
                }
                Console.ReadKey();
            }
        }
    }
}
