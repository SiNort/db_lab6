﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseWF
{
    public static class SQLCommands
    {
        public static SqlCommand GetAdresses(SqlConnection sqlConnection)
        {
            return new SqlCommand("getAdressesFull", sqlConnection);
        }

        public static SqlCommand GetAdressesById(string id, SqlConnection sqlConnection)
        {
            SqlCommand command = new SqlCommand("getAdressesFullById @id", sqlConnection);
            command.Parameters.AddWithValue("@id", id);
            return command;
        }

        public static SqlCommand Select(Tables table, SqlConnection sqlConnection)
        {
            return new SqlCommand($"SELECT * FROM {table}", sqlConnection);
        }

        public static SqlCommand SelectById(Tables table, string id, SqlConnection sqlConnection)
        {
            SqlCommand command = new SqlCommand($"SELECT * FROM {table} WHERE Id = @id", sqlConnection);
            command.Parameters.AddWithValue("@id", id);
            return command;
        }

        public static SqlCommand Count(Tables table, SqlConnection sqlConnection)
        {
            return new SqlCommand($"SELECT Count(*) FROM {table}", sqlConnection);
        }

        public static SqlCommand Update(Tables table, string[] data, SqlConnection sqlConnection)
        {
            SqlCommand command = null;
            switch (table)
            {
                case Tables.Countries:
                    command = new SqlCommand("UPDATE Countries SET Name = @name WHERE Id = @id", sqlConnection);
                    command.Parameters.AddWithValue("@id", data[0]);
                    command.Parameters.AddWithValue("@name", data[1]);
                    break;
                case Tables.Cities:
                    command = new SqlCommand("UPDATE Cities SET Name = @name WHERE Id = @id", sqlConnection);
                    command.Parameters.AddWithValue("@id", data[0]);
                    command.Parameters.AddWithValue("@name", data[1]);
                    break;
                case Tables.Adresses:
                    command = new SqlCommand("UPDATE Adresses SET CountryId = @countryId, CityId = @cityId, StreetId = @streetId, HouseNumber = @houseNum WHERE Id = @id", sqlConnection);
                    command.Parameters.AddWithValue("@id", data[0]);
                    command.Parameters.AddWithValue("@countryId", data[1]);
                    command.Parameters.AddWithValue("@cityId", data[2]);
                    command.Parameters.AddWithValue("@streetId", data[3]);
                    command.Parameters.AddWithValue("@houseNum", data[4]);
                    break;
            }
            return command;
        }

        public static SqlCommand GetId(Tables table, string name, SqlConnection sqlConnection)
        {
            SqlCommand command = null;
            switch (table)
            {
                case Tables.Countries:
                    command = new SqlCommand("getCountryId", sqlConnection);
                    command.CommandType = System.Data.CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@countryName", name);
                    break;
                case Tables.Cities:
                    command = new SqlCommand("getCityId", sqlConnection);
                    command.CommandType = System.Data.CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@cityName", name);
                    break;
                case Tables.Streets:
                    command = new SqlCommand("getStreetId", sqlConnection);
                    command.CommandType = System.Data.CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@streetName", name);
                    break;
            }
            return command;
        }

        public static SqlCommand Delete(Tables table, string id, SqlConnection sqlConnection)
        {
            return new SqlCommand($"DELETE FROM {table} WHERE Id = {id}",  sqlConnection);
        }

        public enum Tables
        {
            Adresses,
            Cities,
            Countries,
            Streets
        }
    }
}
