﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static DatabaseWF.SQLCommands;

namespace DatabaseWF
{
    public class Database
    {
        public delegate void DataHadler(string[] data);
        public DataHadler dataHadler;

        string connectionString;
        public Database(string conString)
        {
            connectionString = conString;
        }

        public void SelecById(DataGridView dataTable, Tables table, string id)
        {
            string[] data = new string[3];
            SqlDataAdapter adapter;
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                data[0] = "Connection... Established";
                if (table == SQLCommands.Tables.Adresses)
                {
                    adapter = new SqlDataAdapter(SQLCommands.GetAdressesById(id, connection));
                }
                else
                {
                    adapter = new SqlDataAdapter(SQLCommands.SelectById(table, id, connection));
                }

                DataSet ds = new DataSet();
                adapter.Fill(ds);
                data[1] = "Data sampling...Complete";
                dataTable.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
                dataTable.AllowUserToAddRows = false;
                dataTable.AllowUserToDeleteRows = false;
                dataTable.DataSource = ds.Tables[0];
                data[2] = "Display... Complete";
                dataHadler(data);
            }
        }

        public void Delete(Tables table, string id)
        {
            SqlCommand command;
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                command = SQLCommands.Delete(table, id, connection);
                command.ExecuteNonQuery();
            }
        }

        public void UpdateAdresses(string[] data)
        {
            SqlCommand command;
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                command = GetId(Tables.Countries, data[1], connection);
                int countryId = Convert.ToInt32(command.ExecuteScalar());
                command = GetId(Tables.Cities, data[2], connection);
                int cityId = Convert.ToInt32(command.ExecuteScalar());
                command = GetId(Tables.Streets, data[3], connection);
                int streetId = Convert.ToInt32(command.ExecuteScalar());
                command = SQLCommands.Update(Tables.Adresses, new string[] { data[0], countryId.ToString(), cityId.ToString(), streetId.ToString(), data[4] }, connection);
                command.ExecuteNonQuery();
            }
        }

        public void UpdateCountries(string[] data)
        {
            SqlCommand command;
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                command = SQLCommands.Update(Tables.Countries, new string[] { data[0], data[1] }, connection);
                command.ExecuteNonQuery();
            }
        }

        public void UpdateCities(string[] data)
        {
            SqlCommand command;
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                command = SQLCommands.Update(Tables.Cities, new string[] { data[0], data[1] }, connection);
                command.ExecuteNonQuery();
            }
        }

        public void FillDataTable(DataGridView dataTable, SQLCommands.Tables table)
        {
            string[] data = new string[3];
            SqlDataAdapter adapter;
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                data[0] = "Connection... Established";
                if(table == SQLCommands.Tables.Adresses)
                {
                    adapter = new SqlDataAdapter(SQLCommands.GetAdresses(connection));
                }
                else
                {
                    adapter = new SqlDataAdapter(SQLCommands.Select(table, connection));
                }

                DataSet ds = new DataSet();
                adapter.Fill(ds);
                data[1] = "Data sampling...Complete";
                dataTable.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
                dataTable.AllowUserToAddRows = false;
                dataTable.AllowUserToDeleteRows = false;
                dataTable.DataSource = ds.Tables[0];
                data[2] = "Display... Complete";
                dataHadler(data);
            }
        }

        public void AddAdress(string[] data)
        {
            SqlCommand command;
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                command = GetId(Tables.Countries, data[0], connection);
                int countryId = Convert.ToInt32(command.ExecuteScalar());
                command = GetId(Tables.Cities, data[1], connection);
                int cityId = Convert.ToInt32(command.ExecuteScalar());
                command = GetId(Tables.Streets, data[2], connection);
                int streetId = Convert.ToInt32(command.ExecuteScalar());
                command = new SqlCommand("INSERT Adresses VALUES(@countryId, @cityId, @streetId, @houseNum)", connection);
                command.Parameters.AddWithValue("@countryId", countryId);
                command.Parameters.AddWithValue("@cityId", cityId);
                command.Parameters.AddWithValue("@streetId", streetId);
                command.Parameters.AddWithValue("@houseNum", data[3]);
                command.ExecuteNonQuery();
            }
        }

        public void AddCity(string name)
        {
            int rowsNum;
            SqlCommand command;
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                command = Count(Tables.Cities, connection);
                rowsNum = Convert.ToInt32(command.ExecuteScalar());

                command = GetId(Tables.Cities, name, connection);
                command.ExecuteNonQuery();

                command = Count(Tables.Cities, connection);

                if (rowsNum == Convert.ToInt32(command.ExecuteScalar()))
                {
                    throw new ArgumentException("The element already in database");
                }
            }
        }

        public void AddCountry(string name)
        {
            int rowsNum;
            SqlCommand command;
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                command = Count(Tables.Countries, connection);
                rowsNum = Convert.ToInt32(command.ExecuteScalar());

                command = GetId(Tables.Countries, name, connection);
                command.ExecuteNonQuery();

                command = Count(Tables.Countries, connection);

                if (rowsNum == Convert.ToInt32(command.ExecuteScalar()))
                {
                    throw new ArgumentException("The element already in database");
                }
            }
        }
    }
}
