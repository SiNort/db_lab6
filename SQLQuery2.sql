﻿CREATE PROCEDURE getStreetId 
	@streetName NVARCHAR(30)
AS
BEGIN
	DECLARE @isStreetInside INT, @result INT

	SELECT @isStreetInside = Count(Streets.Id)
	FROM Streets
	WHERE Streets.Name = @streetName

	IF @isStreetInside >= 1
		BEGIN
			SELECT @result = Streets.Id
			FROM Streets
			WHERE Streets.Name = @streetName
		END
	ELSE 
		BEGIN
			INSERT Streets
			VALUES (@streetName)

			SELECT @result = SCOPE_IDENTITY()
		END
	RETURN @result
END
