﻿using DatabaseWF;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DB_Form
{
    public partial class Form1 : Form
    {
        Database database;
        public Form1()
        {
            InitializeComponent();
            database = new Database(ConfigurationManager.ConnectionStrings["DB"].ConnectionString);
            database.dataHadler += SetLogs;
            dataGridView1.SelectionChanged += DataGridView1_SelectionChanged;
            dataGridView2.SelectionChanged += DataGridView2_SelectionChanged;
            dataGridView3.SelectionChanged += DataGridView3_SelectionChanged;
            database.FillDataTable(dataGridView1, SQLCommands.Tables.Adresses);
            database.FillDataTable(dataGridView2, SQLCommands.Tables.Cities);
            database.FillDataTable(dataGridView3, SQLCommands.Tables.Countries);
        }

        private void DataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                foreach (DataGridViewRow row in dataGridView1.SelectedRows)
                {
                    idField1.Text = row.Cells[0].Value.ToString();
                    countryField.Text = row.Cells[1].Value.ToString();
                    cityField.Text = row.Cells[2].Value.ToString();
                    stretField.Text = row.Cells[3].Value.ToString();
                    houseNumField.Text = row.Cells[4].Value.ToString();
                }
            }
        }

        private void DataGridView2_SelectionChanged(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in dataGridView2.SelectedRows)
            {
                idField2.Text = row.Cells[0].Value.ToString();
                cityNameField.Text = row.Cells[1].Value.ToString();
            }
        }

        private void DataGridView3_SelectionChanged(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in dataGridView3.SelectedRows)
            {
                idField3.Text = row.Cells[0].Value.ToString();
                countryNameField.Text = row.Cells[1].Value.ToString();
            }
        }

        public void SetLogs(string[] data)
        {
            label1.Text = label6.Text = label9.Text = data[0];
            label2.Text = label5.Text = label8.Text = data[1];
            label3.Text = label4.Text = label7.Text = data[2];
        }

        private void button10_Click(object sender, EventArgs e)
        {
            database.FillDataTable(dataGridView1, SQLCommands.Tables.Adresses);
        }

        private void button11_Click(object sender, EventArgs e)
        {
            database.FillDataTable(dataGridView2, SQLCommands.Tables.Cities);
        }

        private void button12_Click(object sender, EventArgs e)
        {
            database.FillDataTable(dataGridView3, SQLCommands.Tables.Countries);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            database.AddAdress(new string[] { countryField.Text, cityField.Text, stretField.Text, houseNumField.Text });
            idField1.Text = "-";
            countryField.Text = "";
            cityField.Text = "";
            stretField.Text = "";
            houseNumField.Text = "";
        }

        private void button6_Click(object sender, EventArgs e)
        {
            try
            {
                database.AddCity(cityNameField.Text);
                idField2.Text = "-";
                cityNameField.Text = "";
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
        }

        private void button9_Click(object sender, EventArgs e)
        {
            try
            {
                database.AddCountry(countryNameField.Text);
                idField3.Text = "-";
                countryNameField.Text = "";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (idField1.Text == "-") throw new ArgumentException("Row is not selected");
                database.Delete(SQLCommands.Tables.Adresses, idField1.Text);
                idField1.Text = "-";
                countryField.Text = "";
                cityField.Text = "";
                stretField.Text = "";
                houseNumField.Text = "";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                if (idField2.Text == "-") throw new ArgumentException("Row is not selected");
                database.Delete(SQLCommands.Tables.Cities, idField2.Text);
                idField2.Text = "-";
                cityNameField.Text = "";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            try
            {
                if (idField3.Text == "-") throw new ArgumentException("Row is not selected");
                database.Delete(SQLCommands.Tables.Cities, idField3.Text);
                idField3.Text = "-";
                countryNameField.Text = "";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                if (idField1.Text == "-") throw new ArgumentException("Row is not selected");
                database.UpdateAdresses(new string[] { idField1.Text, countryField.Text, cityField.Text, stretField.Text, houseNumField.Text });
                idField1.Text = "-";
                countryField.Text = "";
                cityField.Text = "";
                stretField.Text = "";
                houseNumField.Text = "";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            try
            {
                if (idField2.Text == "-") throw new ArgumentException("Row is not selected");
                database.UpdateCities(new string[] { idField2.Text, cityNameField.Text });
                idField2.Text = "-";
                cityNameField.Text = "";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            try
            {
                if (idField3.Text == "-") throw new ArgumentException("Row is not selected");
                database.UpdateCities(new string[] { idField3.Text, countryNameField.Text });
                idField3.Text = "-";
                countryNameField.Text = "";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
        }

        private void button13_Click(object sender, EventArgs e)
        {
            try
            {
                Convert.ToInt32(idSearch1.Text);
                if (idSearch1.Text == "") throw new ArgumentException("Row is not selected");
                database.SelecById(dataGridView1, SQLCommands.Tables.Adresses, idSearch1.Text);
                idSearch1.Text = "";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
        }

        private void button14_Click(object sender, EventArgs e)
        {
            try
            {
                Convert.ToInt32(idSearch2.Text);
                if (idSearch2.Text == "") throw new ArgumentException("Row is not selected");
                database.SelecById(dataGridView2, SQLCommands.Tables.Cities, idSearch2.Text);
                idSearch2.Text = "";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
        }

        private void button15_Click(object sender, EventArgs e)
        {
            try
            {
                Convert.ToInt32(idSearch3.Text);
                if (idSearch3.Text == "") throw new ArgumentException("Row is not selected");
                database.SelecById(dataGridView3, SQLCommands.Tables.Countries, idSearch3.Text);
                idSearch3.Text = "";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
        }
    }
}
