﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseLibrary
{
    public static class DatabaseCommands
    {
        public static SqlCommand Procedure(Procedures procedure, object[] values, SqlConnection connection)
        {
            SqlCommand command = new SqlCommand(procedure.ToString(), connection);
            command.CommandType = System.Data.CommandType.StoredProcedure;

            if(values != null)
            {
                for (int i = 0; i < values.Length; i++)
                {
                    command.Parameters.AddWithValue("value" + i, values[i]);
                }
            }

            return command;
        }

        public static SqlCommand SelectRandom(Tables tableName, SqlConnection connection)
        {
            string sqlExpression = $"SELECT TOP 1 * FROM { tableName } ORDER BY NEWID()";

            SqlCommand command = new SqlCommand(sqlExpression, connection);

            return command;
        }

        public static SqlCommand SelectById(Tables tableName, int id, SqlConnection connection)
        {
            string sqlExpression = $"SELECT * FROM { tableName } WHERE Id = @id";

            SqlCommand command = new SqlCommand(sqlExpression, connection);
            command.Parameters.AddWithValue("@id", id);

            return command;
        }

        public static SqlCommand SelectAll(Tables tableName, SqlConnection connection)
        {
            string sqlExpression = $"SELECT * FROM { tableName }";

            SqlCommand command = new SqlCommand(sqlExpression, connection);

            return command;
        }

        public static SqlCommand CountRows(Tables tableName, SqlConnection connection)
        {
            string sqlExpression = $"SELECT COUNT(*) FROM {tableName}";

            SqlCommand command = new SqlCommand(sqlExpression, connection);

            return command;
        }

        public enum Tables
        {
            Adresses,
            Car_Models,
            Car_Specifications,
            Cars,
            Case_Types,
            Cities,
            Colors,
            Countries,
            Customers,
            Duties,
            Duties_List,
            Employees,
            Full_Names,
            Manufacturers,
            Orders,
            Passports_Data,
            Phone_Numbers,
            Posts,
            Requirements,
            Requirements_List,
            Streets
        }

        public enum Procedures
        {
            AddAdress,
            AddCar,
            AddCarModels,
            AddCarSpecification,
            AddCase,
            AddCity,
            AddColor,
            AddCountry,
            AddCustomers,
            AddDutie,
            AddDutieList,
            AddEmployee,
            AddFullNames,
            AddManufacturer,
            AddOrder,
            AddPassportData,
            AddPhoneNumber,
            AddPost,
            AddRequirements,
            AddRequirementsList,
            AddStreet,
            DeleteCar,
            UpdateCar,
            GetCarsByManufacturer,
            GetCarsPriceByCountry,
            GetEmployeeByPost,
            GetOrders100and200,
            GetOrdersPricesAndDates,
            GetSelledCars
        }
    }
}
