﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseLibrary
{
    public class Database
    {
        private readonly string connectionString;

        public Database(string conString)
        {
            connectionString = conString;
        }
        
        public void VoidProcedure(DatabaseCommands.Procedures procedure, object[] values)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = DatabaseCommands.Procedure(procedure, values, connection);
                int rowNum = command.ExecuteNonQuery();
            }
        }

        public object[] SelectRandom(DatabaseCommands.Tables table)
        {
            object[] data = null;
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = DatabaseCommands.SelectRandom(table, connection);
                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        data = new object[reader.FieldCount];
                        for (int i = 0; i < reader.FieldCount; i++)
                        {
                            data[i] = reader.GetValue(i);
                        }
                    }
                }
            }
            return data;
        }

        public List<object[]> SelectById(DatabaseCommands.Tables table, int id)
        {
            List<object[]> data = new List<object[]>();
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = DatabaseCommands.SelectById(table, id, connection);
                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    List<object> columns = new List<object>();
                    DataTable dataTable = reader.GetSchemaTable();
                    foreach (DataRow row in dataTable.Rows)
                    {
                        columns.Add(row.Field<String>("ColumnName"));
                    }
                    data.Add(columns.ToArray());

                    while (reader.Read())
                    {
                        var objects = new object[reader.FieldCount];
                        for (int i = 0; i < reader.FieldCount; i++)
                        {
                            objects[i] = reader.GetValue(i);
                        }
                        data.Add(objects);
                    }
                }
            }
            return data;
        }

        public List<object[]> SelectProcedure(DatabaseCommands.Procedures procedure, object[] values)
        {
            List<object[]> data = new List<object[]>();
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = DatabaseCommands.Procedure(procedure, values, connection);
                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    List<object> columns = new List<object>();
                    DataTable dataTable = reader.GetSchemaTable();
                    foreach (DataRow row in dataTable.Rows)
                    {
                        columns.Add(row.Field<String>("ColumnName"));
                    }
                    data.Add(columns.ToArray());

                    while (reader.Read())
                    {
                        var objects = new object[reader.FieldCount];
                        for(int i = 0; i < reader.FieldCount; i++)
                        {
                            objects[i] = reader.GetValue(i);
                        }
                        data.Add(objects);
                    }
                }
            }
            return data;
        }

        public object ScalarProcedure(DatabaseCommands.Procedures procedure, object[] values)
        {
            object data = 0;
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                object[] val = new object[values.Length + 1];
                values.CopyTo(val, 0);
                val[val.Length - 1] = 0;
                SqlCommand command = DatabaseCommands.Procedure(procedure, val, connection);
                data = command.ExecuteScalar();
            }
            return data;
        }

        public List<object[]> Select(DatabaseCommands.Tables table, int num = -1)
        {
            List<object[]> data = new List<object[]>();
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = DatabaseCommands.SelectAll(table, connection);
                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    List<object> columns = new List<object>();
                    DataTable dataTable = reader.GetSchemaTable();
                    foreach (DataRow row in dataTable.Rows)
                    {
                        columns.Add(row.Field<String>("ColumnName"));
                    }
                    data.Add(columns.ToArray());

                    while (reader.Read())
                    {
                        var objects = new object[reader.FieldCount];
                        for (int i = 0; i < reader.FieldCount; i++)
                        {
                            objects[i] = reader.GetValue(i);
                        }
                        data.Add(objects);
                        if(data.Count == num + 1)
                        {
                            break;
                        }
                    }
                }
            }
            return data;
        }

        public bool IsEmpty(DatabaseCommands.Tables tableName)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = DatabaseCommands.CountRows(tableName, connection);
                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        if(Convert.ToInt32(reader.GetValue(0)) == 0)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
            }
            return true;
        }
    }
}
