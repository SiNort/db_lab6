﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Drawing;
using System.Text;
using System.Threading.Tasks;
using Tables = DatabaseLibrary.DatabaseCommands.Tables;

namespace DatabaseLibrary
{
    public static class DatabaseInitializer
    {
        public static void Initialize(string conString)
        {
            Random random = new Random();
            Database database = new Database(conString);

            if (database.IsEmpty(Tables.Colors))
            {
                Type colorType = typeof(Color);
                PropertyInfo[] propInfos = colorType.GetProperties(BindingFlags.Static | BindingFlags.DeclaredOnly | BindingFlags.Public);
                foreach (PropertyInfo propInfo in propInfos)
                {
                    database.VoidProcedure(DatabaseCommands.Procedures.AddColor, new object[] { propInfo.Name });
                }
            }

            if (database.IsEmpty(Tables.Car_Specifications))
            {
                for(int i = 0; i < 100; i++)
                {
                    database.VoidProcedure(DatabaseCommands.Procedures.AddCarSpecification, new object[] { random.Next(3000, 10000), random.Next(2, 6), random.Next(50, 150), (2 + random.NextDouble() * (8 - 2)) });
                }
            }

            if (database.IsEmpty(Tables.Car_Models))
            {
                for (int i = 0; i < 100; i++)
                {
                    database.VoidProcedure(DatabaseCommands.Procedures.AddCarModels, new object[] { "Model" + i });
                }
            }

            if (database.IsEmpty(Tables.Case_Types))
            {
                for (int i = 0; i < 100; i++)
                {
                    database.VoidProcedure(DatabaseCommands.Procedures.AddCase, new object[] { "CarCase" + i, "CarCaseDescription" + i });
                }
            }

            if (database.IsEmpty(Tables.Countries))
            {
                foreach (CultureInfo ci in CultureInfo.GetCultures(CultureTypes.SpecificCultures))
                {
                    var ri = new RegionInfo(ci.Name);
                    database.VoidProcedure(DatabaseCommands.Procedures.AddCountry, new object[] { ri.DisplayName });
                }
            }

            if (database.IsEmpty(Tables.Cities))
            {
                for (int i = 0; i < 100; i++)
                {
                    database.VoidProcedure(DatabaseCommands.Procedures.AddCity, new object[] { "City" + i });
                }
            }

            if (database.IsEmpty(Tables.Streets))
            {
                for (int i = 0; i < 100; i++)
                {
                    database.VoidProcedure(DatabaseCommands.Procedures.AddStreet, new object[] { "Street" + i });
                }
            }

            if (database.IsEmpty(Tables.Adresses))
            {
                for (int i = 0; i < 10000; i++)
                {
                    database.VoidProcedure(DatabaseCommands.Procedures.AddAdress, new object[] { database.SelectRandom(Tables.Countries)[0].ToString(), database.SelectRandom(Tables.Cities)[0].ToString(), database.SelectRandom(Tables.Streets)[0].ToString(), random.Next(1, 100).ToString() });
                }
            }

            if (database.IsEmpty(Tables.Manufacturers))
            {
                for (int i = 0; i < 10000; i++)
                {
                    database.VoidProcedure(DatabaseCommands.Procedures.AddManufacturer, new object[] { "ManufacturerName" + i, "ManufacturerDescription" + i, database.SelectRandom(Tables.Adresses)[0].ToString() });
                }
            }

            if (database.IsEmpty(Tables.Cars))
            {
                for (int i = 0; i < 10000; i++)
                {
                    database.VoidProcedure(DatabaseCommands.Procedures.AddCar, new object[] { DateTime.Today.AddDays(-random.Next(20*365)), "EngNum" + i, random.Next(10, 200000), database.SelectRandom(Tables.Car_Models)[0].ToString(), database.SelectRandom(Tables.Manufacturers)[0].ToString(), database.SelectRandom(Tables.Case_Types)[0].ToString(), database.SelectRandom(Tables.Colors)[0].ToString(), database.SelectRandom(Tables.Car_Specifications)[0].ToString() });
                }
            }

            if (database.IsEmpty(Tables.Duties))
            {
                for (int i = 0; i < 100; i++)
                {
                    database.VoidProcedure(DatabaseCommands.Procedures.AddDutie, new object[] { "DutieDescription" + i });
                }
            }

            if (database.IsEmpty(Tables.Requirements))
            {
                for (int i = 0; i < 100; i++)
                {
                    database.VoidProcedure(DatabaseCommands.Procedures.AddRequirements, new object[] { "RequirementsDescription" + i });
                }
            }

            if (database.IsEmpty(Tables.Posts))
            {
                for (int i = 0; i < 100; i++)
                {
                    database.VoidProcedure(DatabaseCommands.Procedures.AddPost, new object[] { "Post" + i, random.Next(1000, 40000) });
                }
            }

            if (database.IsEmpty(Tables.Requirements_List))
            {
                for (int i = 0; i < 1000; i++)
                {
                    database.VoidProcedure(DatabaseCommands.Procedures.AddRequirementsList, new object[] { database.SelectRandom(Tables.Posts)[0].ToString(), database.SelectRandom(Tables.Requirements)[0].ToString() });
                }
            }

            if (database.IsEmpty(Tables.Duties_List))
            {
                for (int i = 0; i < 1000; i++)
                {
                    database.VoidProcedure(DatabaseCommands.Procedures.AddDutieList, new object[] { database.SelectRandom(Tables.Posts)[0].ToString(), database.SelectRandom(Tables.Duties)[0].ToString() });
                }
            }

            if (database.IsEmpty(Tables.Phone_Numbers))
            {
                for (int i = 0; i < 1000; i++)
                {
                    database.VoidProcedure(DatabaseCommands.Procedures.AddPhoneNumber, new object[] { "PhoneNum" + i });
                }
            }

            if (database.IsEmpty(Tables.Full_Names))
            {
                for (int i = 0; i < 1000; i++)
                {
                    database.VoidProcedure(DatabaseCommands.Procedures.AddFullNames, new object[] { "Surname" + i, "Name" + i, "LastName" + i });
                }
            }

            if (database.IsEmpty(Tables.Passports_Data))
            {
                for (int i = 0; i < 1000; i++)
                {
                    database.VoidProcedure(DatabaseCommands.Procedures.AddPassportData, new object[] { "PassportSeries" + i, "PassportNumber" + i });
                }
            }

            if (database.IsEmpty(Tables.Customers))
            {
                for (int i = 0; i < 10000; i++)
                {
                    database.VoidProcedure(DatabaseCommands.Procedures.AddCustomers, new object[] { database.SelectRandom(Tables.Full_Names)[0].ToString(), database.SelectRandom(Tables.Phone_Numbers)[0].ToString(), database.SelectRandom(Tables.Adresses)[0].ToString(), database.SelectRandom(Tables.Passports_Data)[0].ToString() });
                }
            }

            if (database.IsEmpty(Tables.Employees))
            {
                for (int i = 0; i < 10000; i++)
                {
                    database.VoidProcedure(DatabaseCommands.Procedures.AddEmployee, new object[] { database.SelectRandom(Tables.Full_Names)[0].ToString(),
                        random.Next(18, 65), random.Next(10) > 5 ? "M" : "F", database.SelectRandom(Tables.Phone_Numbers)[0].ToString(), database.SelectRandom(Tables.Adresses)[0].ToString(), database.SelectRandom(Tables.Passports_Data)[0].ToString(), database.SelectRandom(Tables.Posts)[0].ToString() });
                }
            }

            if (database.IsEmpty(Tables.Orders))
            {
                for (int i = 0; i < 10000; i++)
                {
                    DateTime dateTime = DateTime.Today.AddDays(-random.Next(20 * 365));
                    database.VoidProcedure(DatabaseCommands.Procedures.AddOrder, new object[] { database.SelectRandom(Tables.Customers)[0].ToString(),
                        database.SelectRandom(Tables.Employees)[0].ToString(), database.SelectRandom(Tables.Cars)[0].ToString(), dateTime.AddDays(-200), dateTime, random.Next(10) > 5 ? true : false, random.Next(10) > 5 ? true : false, random.Next(20, 80)});
                }
            }
        }
    }
}
